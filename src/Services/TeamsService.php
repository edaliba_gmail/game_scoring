<?php namespace App\Services;

use Doctrine\ORM\EntityManagerInterface;
use App\Services\Entropy\EntropyInterface;




/**
*	TeamService
*/


class TeamsService {

	protected $em;


	public function __construct(EntityManagerInterface $em) 
	{
		$this->em = $em;
	}



	/**
	*	update
	*/

	public function update($members) 
	{
		foreach($members as $items) {

			list($division, $teams) = $items;

			foreach($teams as $team) {

				$matrix = new \App\Entity\Matrix();
				$matrix->setDivision($division);
				$matrix->setTeam($team);

				$this->em->persist($matrix);
				$this->em->flush();
			} 
		}
	}


	/**
	*	reset
	*/

	public function reset()
	{
		$this->em->createQuery('DELETE App:Matrix m')->execute();
		$this->em->createQuery('DELETE App:Results r')->execute();
		$this->em->createQuery('DELETE App:Rank r')->execute();
	}


	/**
	*	createDivisionMatrix
	*/

	public function createDivisionMatrix(EntropyInterface $entropy)
	{
		$divisions = $this->getDivisions();
		$teams = $this->getTeams();

		return $entropy->split($divisions, $teams);
	}


	/**
	*	getDivisions
	*/

	protected function getDivisions($where = [])
	{
		return $this->em->getRepository("App:Divisions")->findAll();
	}


	/**
	*	getTeams
	*/

	protected function getTeams($where = [])
	{
		return $this->em->getRepository("App:Teams")->findAll();
	}

}