<?php namespace App\Services;

use Doctrine\ORM\EntityManagerInterface;

class GameFactory {

	const GAME_DIVISION = "division";
	const GAME_PLAYOFF = "playoff";
	const GAME_SEMI = "semi";
	const GAME_FINAL = "final";

	/** @var $handlers */
	protected static $handlers = [
		'division' 	=> Games\GameDivision::class,
		'playoff'	=> Games\GamePlayOff::class,
		'semi'		=> Games\GameSemiFinal::class,
		'final'		=> Games\GameFinal::class,
	];

	/** @var $em */
	protected $em;


	public function __construct(EntityManagerInterface $em) 
	{
		$this->em = $em;
	}


	/**
	*	create
	*
	*	@param string $type
	*/

	public function create($type)
	{
		if(isset(GameFactory::$handlers[$type]) && class_exists(GameFactory::$handlers[$type])) {

			// create game
			$game = new GameFactory::$handlers[$type]($this->em);
			$game->setFactory($this);

			return $game;

		} else {

			throw new \Exception("Game handler[".$type."] not found");
		}
	}

}