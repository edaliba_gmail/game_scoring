<?php namespace App\Services\Entropy;

interface EntropyInterface {
	
	public function split($divisions = [], $teams = []);

}