<?php namespace App\Services\Entropy;

class EntropyDivideHalf implements EntropyInterface {


	/**
	*	split
	*
	*	@param Divisions[] $divisions
	*	@param Teams[] $teams
	*	@return array $matrix 
	*/

	public function split($divisions = [], $teams = [])
	{
		$matrix = [];

		$chunks = floor(count($teams) / count($divisions)); 

		foreach($divisions as $n => $division) {

			$matrix[] = [$division, array_splice($teams, 0, $chunks)]; 
		}

		return $matrix;
	} 
}