<?php namespace App\Services\Games;

use Doctrine\ORM\EntityManagerInterface;
use App\Services\Ranks\RankInterface;
use App\Services\GameFactory;
use App\Entity\{Results, Rank};

class GameFinal extends GameAbstract {

	public static $TYPE = 'final';


	/**
	*	selectTeams
	*/

	public function selectTeams()
	{
		$items = [];

		$game = $this->factory->create(GameFactory::GAME_SEMI);

		$ranks = $game->getRank();

		for($n=0; $n<count($ranks); $n+=2) {
			
			$rank1 = $ranks[$n];
			$rank2 = $ranks[$n+1];

			$items[] = [
				'type' => self::$TYPE,
				'team_1' => $rank1->getTeam(),
				'team_2' => $rank2->getTeam(),
			];
		}
		
		return $items;
	}



	/**
	*	rank
	*	@param RankInterface $ranks
	*/

	public function rank(RankInterface $ranks = null) 
	{

		$plays = $this->getPlays();
		$teams = $ranks->rank($plays);

		// set ranks
		$this->setRank($teams);
	}


}