<?php namespace App\Services\Games;

use Doctrine\ORM\EntityManagerInterface;
use App\Services\Ranks\RankInterface;
use App\Services\GameFactory;
use App\Entity\{Results, Rank};

class GameSemiFinal extends GameAbstract {

	public static $TYPE = 'semi';


	/**
	*	selectTeams
	*/

	public function selectTeams()
	{
		$items = [];

		$game = $this->factory->create(GameFactory::GAME_PLAYOFF);

		$ranks = $game->getRank();
		$length = count($ranks) / 2;

		$ranks1 = array_splice($ranks, 0, $length);
		$ranks2 = array_splice($ranks, 0, $length);

		foreach($ranks1 as $n => $rank1) {
			
			$rank2 = $ranks2[$n];

			$items[] = [
				'type' => self::$TYPE,
				'team_1' => $rank1->getTeam(),
				'team_2' => $rank2->getTeam(),
			];
		}
		
		return $items;
	}


	/**
	*	rank
	*	@param RankInterface $ranks
	*/

	public function rank(RankInterface $ranks = null) 
	{
		$teams = [];
		
		$plays = $this->getPlays();
		$teams = $ranks->rank($plays);

		// set ranks
		$this->setRank($teams);
	}

}