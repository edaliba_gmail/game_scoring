<?php namespace App\Services\Games;

interface GameIterface {
	
	public function play();

	public function rank();

}