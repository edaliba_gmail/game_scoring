<?php namespace App\Services\Games;

use Doctrine\ORM\EntityManagerInterface;
use App\Services\Ranks\RankInterface;
use App\Services\GameFactory;
use App\Entity\{Results, Rank};

class GamePlayOff extends GameAbstract {

	public static $TYPE = 'playoff';


	/**
	*	selectTeams
	*/

	public function selectTeams()
	{
		$items = [];

		$game = $this->factory->create(GameFactory::GAME_DIVISION);

		// best from Division A
		$ranks1 = array_slice($game->getRankByDivision(1), 0, 4);
		// best (but reversed) from Division B
		$ranks2 = array_reverse(array_slice($game->getRankByDivision(2), 0, 4));


		foreach($ranks1 as $n => $rank1) {
			
			$rank2 = $ranks2[$n];

			$items[] = [
				'type' => self::$TYPE,
				'team_1' => $rank1->getTeam(),
				'team_2' => $rank2->getTeam(),
			];
		}
		
		return $items;
	}


	/**
	*	rank
	*	@param RankInterface $ranks
	*/

	public function rank(RankInterface $ranks = null)
	{
		$plays = $this->getPlays();
		$teams = $ranks->rank($plays);

		// set ranks
		$this->setRank($teams);		
	}	


}