<?php namespace App\Services\Games;

use Doctrine\ORM\EntityManagerInterface;
use App\Services\Ranks\RankInterface;
use App\Entity\{Results, Rank};

class GameDivision extends GameAbstract {

	public static $TYPE = 'division';


	/**
	*	selectTeams
	*/

	public function selectTeams()
	{
		$items = [];

		$divisions = $this->em->getRepository("App:Divisions")->findAll();

		foreach($divisions as $division) {

			$teams = $this->em->getRepository("App:Matrix")->findByDivision($division->getId());

			foreach($teams as $team_1) {
				foreach($teams as $team_2) {

					if($team_1->getid() == $team_2->getId())
						continue;

					$items[] = [
						'type' => self::$TYPE,
						'team_1' => $team_1->getTeam(),
						'team_2' => $team_2->getTeam()
					];
				}
			}

		}

		return $items;
	}



	/**
	*	rank
	*	@param RankInterface $ranks
	*/

	public function rank(RankInterface $ranks = null) 
	{

		$plays = $this->getPlays();
		$teams = $ranks->rank($plays);

		// set ranks
		$this->setRank($teams);
	}


	/**
	*	getPlaysByDivision
	*	@param int $division_id
	*
	*	@deprecated Jāpārnes uz App/Repositories
	*/

	public function getPlaysByDivision($division_id)
	{
		$plays = $this->getPlays();
		$division = $this->em->getRepository('App:Matrix')->findByDivision($division_id);

		// get division teams, id
		$teams = array_map(function($one){
			return $one->getTeam()->getId();
		}, $division);

		// filter division
		return array_filter($plays, function($one) use ($teams) {
			return in_array($one->getTeam1()->getId(), $teams) ? true : false;
		});
	}


	/**
	*	getRankByDivision
	*	@param int $division_id
	*
	*	@deprecated Jāpārnes uz App/Repositories
	*/

	public function getRankByDivision($division_id)
	{
		$ranks = $this->getRank();
		$division = $this->em->getRepository('App:Matrix')->findByDivision($division_id);

		// get division teams, id
		$teams = array_map(function($one){
			return $one->getTeam()->getId();
		}, $division);

		// filter division
		return array_filter($ranks, function($one) use ($teams) {
			return in_array($one->getTeam()->getId(), $teams) ? true : false;
		});
	}



}