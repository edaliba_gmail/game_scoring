<?php namespace App\Services\Games;

use Doctrine\ORM\EntityManagerInterface;
use App\Services\Ranks\RankInterface;
use App\Services\GameFactory;
use App\Entity\{Results, Rank};

abstract class GameAbstract  {

	protected $em;
	protected $factory;

	public function __construct(EntityManagerInterface $em)
	{
		$this->em = $em;
	}


	/**
	*	setFactory
	*	@param GameFactory $factory
	*/

	public function setFactory(GameFactory $factory)
	{
		$this->factory = $factory;
	}


	/**
	*	play
	*/

	public function play() 
	{

		$plays = $this->getPlays();

		foreach($plays as $play) {

			$team_1 = $play->getTeam1();
			$team_2 = $play->getTeam2();

			$play->setTeamResult1(rand(0,1));
			$play->setTeamResult2(rand(0,1));

			$this->em->persist($play);
			$this->em->flush();			
		}
	}


	/**
	*	rank
	*	@param RankInterface $ranks
	*/

	public function rank(RankInterface $ranks = null)
	{
		// individual implementation
	}


	/**
	*	selectTeams
	*/

	public function selectTeams()
	{
		// individual implementation
	}


	/**
	*	resetPlays
	*/

	public function resetPlays()
	{
		$caller = get_called_class();

		$qb = $this->em->createQueryBuilder()
		->delete('App:Results', 'r')
		->where('r.type = :type')
		->setParameter('type', $caller::$TYPE)
		->getQuery()
        ->execute();

        // reset rank
		$this->resetRank();       
	}


	/**
	*	getPlays
	*	@return Entity $entity
	*/

	public function getPlays()
	{
		$caller = get_called_class();

		return $this->em->getRepository('App:Results')->findBy(['type' => $caller::$TYPE]);
	}


	/**
	*	setPlays
	*	@param array $teams
	*/

	public function setPlays($teams = [])
	{
		foreach($teams as $one) {

			$results = new Results();
			$results->setType($one['type']);
			$results->setTeam1($one['team_1']);
			$results->setTeam2($one['team_2']);

			$this->em->persist($results);
			$this->em->flush();
		}
	}


	/**
	*	resetRank
	*/

	public function resetRank()
	{
		$caller = get_called_class();

		$qb = $this->em->createQueryBuilder()
		->delete('App:Rank', 'r')
		->where('r.type = :type')
		->setParameter('type', $caller::$TYPE)
		->getQuery()
        ->execute();
	}


	/**
	*	setRank
	*/

	public function setRank($teams)
	{
		$caller = get_called_class();

		foreach($teams as $team_id => $score) {

			$team = $this->em->getRepository('App:Teams')->find(['id' => $team_id]);

			$rank = new Rank();
			$rank->setType($caller::$TYPE);
			$rank->setTeam($team);
			$rank->setScore($score);

			$this->em->persist($rank);
			$this->em->flush();				
		}		
	}


	/**
	*	getRank
	*	@return Entity $entity
	*/

	public function getRank()
	{
		$caller = get_called_class();

		return $this->em->getRepository('App:Rank')->findBy(['type' => $caller::$TYPE]);		
	}

}