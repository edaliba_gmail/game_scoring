<?php namespace App\Services\Ranks;


class RankBest implements RankInterface {

	/**
	*	rank
	*
	*	@param Results[] $plays
	*/

	public function rank($plays = []) 
	{

		$teams = [];
		
		foreach($plays as $play) {

			if($play->getTeamResult1() > $play->getTeamResult2()) {
				$teams[$play->getTeam1()->getId()][] = $play->getTeamResult1(); 				
			} else {
				$teams[$play->getTeam2()->getId()][] = $play->getTeamResult2(); 				
			}

			// @TODO: Ja 0:0 ???
		}

		foreach($teams as $team_id => $results) {
			$teams[$team_id] = array_sum($results);
		}

		arsort($teams);

		return $teams;
	}
}







