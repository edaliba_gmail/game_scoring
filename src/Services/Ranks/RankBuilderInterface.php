<?php namespace App\Services\Ranks;

interface RankBuilderInterface {
	
	public function addRank(RankInterface $rank);

}