<?php namespace App\Services\Ranks;


class RankDefault implements RankInterface {

	/**
	*	rank
	*
	*	@param Results[] $plays
	*/

	public function rank($plays = []) 
	{

		$teams = [];
		
		foreach($plays as $play) {

			$teams[$play->getTeam1()->getId()][] = $play->getTeamResult1(); 
			$teams[$play->getTeam2()->getId()][] = $play->getTeamResult2();
		}

		foreach($teams as $team_id => $results) {
			$teams[$team_id] = array_sum($results);
		}

		arsort($teams);

		return $teams;
	}
}