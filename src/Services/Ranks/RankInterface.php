<?php namespace App\Services\Ranks;

interface RankInterface {
	
	public function rank($items = []);

}