<?php namespace App\Repository;

use App\Entity\{Teams, Matrix};
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;


class MatrixRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Matrix::class);
    }


    /**
    *   findByDivision
    */

    public function findByDivision($division_id): array
    {

        return $this->createQueryBuilder('m')
            ->where('m.division = :division_id')
            ->setParameter('division_id', $division_id)
            ->getQuery()
            ->execute();

    }      
}