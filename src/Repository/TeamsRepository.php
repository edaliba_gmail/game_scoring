<?php namespace App\Repository;

use App\Entity\{Teams};
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;


class TeamsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Teams::class);
    }


    /*
    public function findByDivision($division_id): array
    {

        $sql = 'SELECT 
                    t.* 
                FROM matrix m 
                INNER JOIN teams t ON t.id = m.team_id
                WHERE m.division_id = :division_id';

        $params = [
            'division_id' => $division_id,
        ];

        return $this->getEntityManager()->getConnection()->executeQuery($sql, $params)->fetchAll();
    } 
    */   
}