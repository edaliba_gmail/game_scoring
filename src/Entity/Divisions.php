<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Divisions
 *
 * @ORM\Table(name="divisions")
 * @ORM\Entity
 */
class Divisions
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="division", type="string", length=255, nullable=true)
     */
    private $division;

    public function getId(): ?int
    {
        return $this->id;
    }


    /**
    *   getDivision
    */

    public function getDivision(): ?string
    {
        return $this->division;
    }


    /**
    *   setDivision
    */

    public function setDivision(?string $division): self
    {
        $this->division = $division;

        return $this;
    }


}
