<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Results
 *
 * @ORM\Table(name="results", indexes={@ORM\Index(name="team_1", columns={"team_1"}), @ORM\Index(name="team_2", columns={"team_2"})})
 * @ORM\Entity
 */
class Results
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="type", type="string", length=16, nullable=true)
     */
    private $type;

    /**
     * @var int|null
     *
     * @ORM\Column(name="team_result_1", type="integer", nullable=true)
     */
    private $teamResult1;

    /**
     * @var int|null
     *
     * @ORM\Column(name="team_result_2", type="integer", nullable=true)
     */
    private $teamResult2;

    /**
     * @var \Teams
     *
     * @ORM\ManyToOne(targetEntity="Teams")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="team_1", referencedColumnName="id")
     * })
     */
    private $team1;

    /**
     * @var \Teams
     *
     * @ORM\ManyToOne(targetEntity="Teams")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="team_2", referencedColumnName="id")
     * })
     */
    private $team2;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getTeamResult1(): ?int
    {
        return $this->teamResult1;
    }

    public function setTeamResult1(?int $teamResult1): self
    {
        $this->teamResult1 = $teamResult1;

        return $this;
    }

    public function getTeamResult2(): ?int
    {
        return $this->teamResult2;
    }

    public function setTeamResult2(?int $teamResult2): self
    {
        $this->teamResult2 = $teamResult2;

        return $this;
    }

    public function getTeam1(): ?Teams
    {
        return $this->team1;
    }

    public function setTeam1(?Teams $team1): self
    {
        $this->team1 = $team1;

        return $this;
    }

    public function getTeam2(): ?Teams
    {
        return $this->team2;
    }

    public function setTeam2(?Teams $team2): self
    {
        $this->team2 = $team2;

        return $this;
    }


}
