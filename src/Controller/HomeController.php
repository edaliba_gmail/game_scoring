<?php namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use App\Services\{TeamsService, GameFactory};
use App\Services\Entropy\EntropyDivideHalf;
use App\Services\Ranks\{RankBuilder, RankDefault, RankBest};


class HomeController extends Controller
{

	/**
     * @Route("/", name="home")
     */

	public function index(TeamsService $ts, GameFactory $factory)
	{

		// $em = $this->getDoctrine()->getManager();



		// Divīzijas spēle
		$game = $factory->create(GameFactory::GAME_DIVISION);
		
		$plays[GameFactory::GAME_DIVISION] = [
			$game->getPlaysByDivision(1),
			$game->getPlaysByDivision(2)
		];

		$ranks[GameFactory::GAME_DIVISION] = [
			$game->getRankByDivision(1),
			$game->getRankByDivision(2)
		];

		// Play Off
		$game = $factory->create(GameFactory::GAME_PLAYOFF);
		
		$plays[GameFactory::GAME_PLAYOFF] = $game->getPlays();
		$ranks[GameFactory::GAME_PLAYOFF] = $game->getRank();


		// Semi
		$game = $factory->create(GameFactory::GAME_SEMI);
		
		$plays[GameFactory::GAME_SEMI] = $game->getPlays();
		$ranks[GameFactory::GAME_SEMI] = $game->getRank();


		// Final
		$game = $factory->create(GameFactory::GAME_FINAL);
		
		$plays[GameFactory::GAME_FINAL] = $game->getPlays();
		$ranks[GameFactory::GAME_FINAL] = $game->getRank();		


	    return $this->render('index.html.twig', array(
	    	'plays' => $plays,
	    	'ranks' => $ranks,
	    ));	    
	}


	/**
     * @Route("/prepare", name="prepare")
     */

	public function prepare(TeamsService $service, GameFactory $factory)
	{

		// set matrix
		$service->reset();
		
		// Kā dalīsim komandas?
		$entropy = new EntropyDivideHalf();

		$matrix = $service->createDivisionMatrix($entropy);
		$service->update($matrix);


		try {

			// atlasām komandas nākošai spēlei
			$next = $factory->create(GameFactory::GAME_DIVISION);
			
			$teams = $next->selectTeams();

			$next->resetPlays();
			$next->setPlays($teams);	

		} catch (\Exception $e) {
			die($e->getMessage());
		}

		return $this->redirectToRoute('home');
	}


	/**
     * @Route("/play1", name="play1")
     */

	public function play1(TeamsService $service, GameFactory $factory)
	{

		try {

			/*
			// Šo varētu izmantot, lai piem., kārtotu tās spēles, kurām vienāds punktu skaits
			// @todo :)

			$rb = new RankBuilder();
			$rb->addRank(new RankDefault());
			$rb->addRank(new RankTotals());
			*/

			// kā kārtosim spēļu rezultātus?
			$rb = new RankDefault();

			$game = $factory->create(GameFactory::GAME_DIVISION);

			$game->play();
			
			$game->resetRank();
			$game->rank($rb);

		} catch (\Exception $e) {
			die($e->getMessage());
		}



		try {

			// atlasām komandas nākošai spēlei
			$next = $factory->create(GameFactory::GAME_PLAYOFF);
			
			$teams = $next->selectTeams();

			$next->resetPlays();
			$next->setPlays($teams);	

		} catch (\Exception $e) {
			die($e->getMessage());
		}

		return $this->redirectToRoute('home');
	}



	/**
     * @Route("/play2", name="play2")
     */

	public function play2(TeamsService $service, GameFactory $factory)
	{

		try {

			$game = $factory->create(GameFactory::GAME_PLAYOFF);

			$game->play();
			
			$game->resetRank();
			$game->rank(new RankBest());

		} catch (\Exception $e) {
			die($e->getMessage());
		}



		try {

			// atlasām komandas nākošai spēlei
			$next = $factory->create(GameFactory::GAME_SEMI);
			
			$teams = $next->selectTeams();

			$next->resetPlays();
			$next->setPlays($teams);	

		} catch (\Exception $e) {
			die($e->getMessage());
		}

		return $this->redirectToRoute('home');
	}




	/**
     * @Route("/play3", name="play3")
     */

	public function play3(TeamsService $service, GameFactory $factory)
	{

		try {

			$game = $factory->create(GameFactory::GAME_SEMI);

			$game->play();
			
			$game->resetRank();
			$game->rank(new RankDefault());

		} catch (\Exception $e) {
			die($e->getMessage());
		}



		try {

			// atlasām komandas nākošai spēlei
			$next = $factory->create(GameFactory::GAME_FINAL);
			
			$teams = $next->selectTeams();

			$next->resetPlays();
			$next->setPlays($teams);	

		} catch (\Exception $e) {
			die($e->getMessage());
		}

		return $this->redirectToRoute('home');
	}






	/**
     * @Route("/play4", name="play4")
     */

	public function play4(TeamsService $service, GameFactory $factory)
	{

		try {

			$game = $factory->create(GameFactory::GAME_FINAL);

			$game->play();
			
			$game->resetRank();
			$game->rank(new RankDefault());

		} catch (\Exception $e) {
			die($e->getMessage());
		}

		return $this->redirectToRoute('home');
	}






}