/*
MySQL Backup
Source Server Version: 10.1.16
Source Database: game
Date: 11.07.2018 16:41:56
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
--  Table structure for `divisions`
-- ----------------------------
DROP TABLE IF EXISTS `divisions`;
CREATE TABLE `divisions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `division` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `matrix`
-- ----------------------------
DROP TABLE IF EXISTS `matrix`;
CREATE TABLE `matrix` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `team_id` int(11) DEFAULT NULL,
  `division_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `division_id` (`division_id`),
  KEY `team_id` (`team_id`) USING BTREE,
  CONSTRAINT `matrix_ibfk_1` FOREIGN KEY (`team_id`) REFERENCES `teams` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `matrix_ibfk_2` FOREIGN KEY (`division_id`) REFERENCES `divisions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=305 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `rank`
-- ----------------------------
DROP TABLE IF EXISTS `rank`;
CREATE TABLE `rank` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(16) DEFAULT NULL,
  `team_id` int(11) DEFAULT NULL,
  `score` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `team_id` (`team_id`),
  CONSTRAINT `rank_ibfk_1` FOREIGN KEY (`team_id`) REFERENCES `teams` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=322 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `results`
-- ----------------------------
DROP TABLE IF EXISTS `results`;
CREATE TABLE `results` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(16) DEFAULT NULL,
  `team_1` int(11) DEFAULT NULL,
  `team_2` int(11) DEFAULT NULL,
  `team_result_1` int(255) DEFAULT NULL,
  `team_result_2` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `team_1` (`team_1`),
  KEY `team_2` (`team_2`),
  CONSTRAINT `results_ibfk_1` FOREIGN KEY (`team_1`) REFERENCES `teams` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `results_ibfk_2` FOREIGN KEY (`team_2`) REFERENCES `teams` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1379 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `teams`
-- ----------------------------
DROP TABLE IF EXISTS `teams`;
CREATE TABLE `teams` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `team` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records 
-- ----------------------------
INSERT INTO `divisions` VALUES ('1','Division A'), ('2','Division B');
INSERT INTO `matrix` VALUES ('289','1','1'), ('290','2','1'), ('291','3','1'), ('292','4','1'), ('293','5','1'), ('294','6','1'), ('295','7','1'), ('296','8','1'), ('297','9','2'), ('298','10','2'), ('299','11','2'), ('300','12','2'), ('301','13','2'), ('302','14','2'), ('303','15','2'), ('304','16','2');
INSERT INTO `rank` VALUES ('294','division','2','10'), ('295','division','3','10'), ('296','division','1','9'), ('297','division','10','9'), ('298','division','11','9'), ('299','division','14','9'), ('300','division','12','8'), ('301','division','16','8'), ('302','division','5','7'), ('303','division','6','7'), ('304','division','9','7'), ('305','division','13','6'), ('306','division','15','5'), ('307','division','7','4'), ('308','division','8','4'), ('309','division','4','3'), ('310','playoff','12','1'), ('311','playoff','14','1'), ('312','playoff','1','1'), ('313','playoff','10','1'), ('314','semi','12','1'), ('315','semi','14','1'), ('316','semi','1','0'), ('317','semi','10','0'), ('318','final','1','1'), ('319','final','14','1'), ('320','final','12','0'), ('321','final','10','0');
INSERT INTO `results` VALUES ('1259','division','1','2','1','1'), ('1260','division','1','3','0','1'), ('1261','division','1','4','0','0'), ('1262','division','1','5','1','1'), ('1263','division','1','6','0','0'), ('1264','division','1','7','1','0'), ('1265','division','1','8','1','0'), ('1266','division','2','1','0','0'), ('1267','division','2','3','1','1'), ('1268','division','2','4','1','0'), ('1269','division','2','5','1','1'), ('1270','division','2','6','1','1'), ('1271','division','2','7','1','1'), ('1272','division','2','8','1','0'), ('1273','division','3','1','0','1'), ('1274','division','3','2','1','1'), ('1275','division','3','4','1','0'), ('1276','division','3','5','0','1'), ('1277','division','3','6','1','1'), ('1278','division','3','7','1','0'), ('1279','division','3','8','0','0'), ('1280','division','4','1','1','0'), ('1281','division','4','2','1','1'), ('1282','division','4','3','0','1'), ('1283','division','4','5','0','1'), ('1284','division','4','6','0','0'), ('1285','division','4','7','0','0'), ('1286','division','4','8','1','1'), ('1287','division','5','1','0','1'), ('1288','division','5','2','0','0'), ('1289','division','5','3','1','1'), ('1290','division','5','4','1','0'), ('1291','division','5','6','0','1'), ('1292','division','5','7','1','1'), ('1293','division','5','8','0','0'), ('1294','division','6','1','1','1'), ('1295','division','6','2','1','0'), ('1296','division','6','3','1','1'), ('1297','division','6','4','0','0'), ('1298','division','6','5','0','0'), ('1299','division','6','7','0','0'), ('1300','division','6','8','1','1'), ('1301','division','7','1','0','1'), ('1302','division','7','2','1','1'), ('1303','division','7','3','0','1'), ('1304','division','7','4','0','0'), ('1305','division','7','5','1','0'), ('1306','division','7','6','0','0'), ('1307','division','7','8','0','0'), ('1308','division','8','1','0','1'), ('1309','division','8','2','1','0'), ('1310','division','8','3','0','0'), ('1311','division','8','4','0','0'), ('1312','division','8','5','1','0'), ('1313','division','8','6','0','0'), ('1314','division','8','7','0','0'), ('1315','division','9','10','0','1'), ('1316','division','9','11','1','1'), ('1317','division','9','12','1','1'), ('1318','division','9','13','1','0'), ('1319','division','9','14','1','1'), ('1320','division','9','15','1','1'), ('1321','division','9','16','0','1'), ('1322','division','10','9','1','0'), ('1323','division','10','11','0','1'), ('1324','division','10','12','0','1'), ('1325','division','10','13','1','1'), ('1326','division','10','14','0','1'), ('1327','division','10','15','1','0'), ('1328','division','10','16','0','0'), ('1329','division','11','9','1','0'), ('1330','division','11','10','0','1'), ('1331','division','11','12','1','0'), ('1332','division','11','13','1','1'), ('1333','division','11','14','1','1'), ('1334','division','11','15','0','0'), ('1335','division','11','16','0','0'), ('1336','division','12','9','0','0'), ('1337','division','12','10','0','1'), ('1338','division','12','11','1','1'), ('1339','division','12','13','0','0'), ('1340','division','12','14','0','1'), ('1341','division','12','15','1','1'), ('1342','division','12','16','1','1'), ('1343','division','13','9','1','1'), ('1344','division','13','10','1','1'), ('1345','division','13','11','0','0'), ('1346','division','13','12','0','0'), ('1347','division','13','14','0','1'), ('1348','division','13','15','0','0'), ('1349','division','13','16','1','1'), ('1350','division','14','9','0','0'), ('1351','division','14','10','0','1'), ('1352','division','14','11','1','1'), ('1353','division','14','12','0','1'), ('1354','division','14','13','1','0'), ('1355','division','14','15','0','0'), ('1356','division','14','16','1','1'), ('1357','division','15','9','0','1'), ('1358','division','15','10','0','0');
INSERT INTO `results` VALUES ('1359','division','15','11','1','1'), ('1360','division','15','12','0','1'), ('1361','division','15','13','0','1'), ('1362','division','15','14','0','1'), ('1363','division','15','16','1','1'), ('1364','division','16','9','1','0'), ('1365','division','16','10','0','1'), ('1366','division','16','11','1','0'), ('1367','division','16','12','0','1'), ('1368','division','16','13','0','0'), ('1369','division','16','14','1','0'), ('1370','division','16','15','0','1'), ('1371','playoff','2','12','1','1'), ('1372','playoff','3','14','0','1'), ('1373','playoff','1','11','1','0'), ('1374','playoff','5','10','0','1'), ('1375','semi','12','1','1','0'), ('1376','semi','14','10','1','0'), ('1377','final','12','14','0','1'), ('1378','final','1','10','1','0');
INSERT INTO `teams` VALUES ('1','A'), ('2','B'), ('3','C'), ('4','D'), ('5','E'), ('6','F'), ('7','G'), ('8','H'), ('9','I'), ('10','J'), ('11','K'), ('12','L'), ('13','M'), ('14','N'), ('15','O'), ('16','P');
